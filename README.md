# Description  
A quick and easy OpenVPN setup utilizing Docker and systemd  

# Installation  
Follow these steps: https://github.com/kylemanna/docker-openvpn  
Use `OVPN_DATA="ovpn-data-volume"`  

# Quick Start  
`docker run -v ovpn-data-volume:/etc/openvpn -d -p 1194:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn`  

# A systemd unit  
```
[Unit]
Description=ZekOpenVPN
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=30
ExecStartPre=-/usr/bin/docker kill zek_openvpn1
ExecStartPre=-/usr/bin/docker rm zek_openvpn1
ExecStartPre=-/usr/bin/docker pull kylemanna/openvpn
ExecStart=/usr/bin/docker run --name=zek_openvpn1 -v ovpn-data-volume:/etc/openvpn -p 1194:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn

[Install]
WantedBy=multi-user.target
```
